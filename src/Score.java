import javafx.scene.paint.Color;

public class Score {
    
    private int score;
    private int vie;

    /*
     * Construit un nouveau Score
     */
    public Score(){
        this.score = 0;
        this.vie = 3;
    }
    
    /*
     * retourne le score
     * 
     * @return int
     */
    public int getScore(){
        return this.score;
    }

    /*
     * retourne le nombre de vie restantes
     * 
     * @return int
     */
    public int getVie(){
        return this.vie;
    }

    /*
     * ajoute un nombre de points au score
     * 
     * @param int points
     */
    public void ajoute(int points){
        this.score += points;
    }

    /*
     * soustrait un nombre de points au vie
     * 
     * @param int points
     */
    public void perd(int points){
        this.vie -= points;
    }

    /*
     * ajoute 300 points par vie restantes
     */
    public void ajoutePointsParVie(){
        int i = 0;
        while(getVie() != i){
            this.score += 500;
            perd(1);
        }

        perd(-3);
    }

    /*
     * retourne un EnsembleChaines contenant le score
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaine = new EnsembleChaines();
        chaine.ajouteChaine(5, 57, ("Score : "+this.score+" - Vie : "+this.vie), Color.YELLOW);
        return chaine;
    }
}
