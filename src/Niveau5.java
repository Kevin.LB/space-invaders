import javafx.scene.paint.Color;

public class Niveau5 {
    private double posX;
    private double posY;

    /*
     * construit un nouvel objet Niveau5
     * 
     * @param double posX
     * @param double posY
     */
    public Niveau5(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    /*
     * retourne un EnsembleChaines contenant le message affiché lorsque l'utisateur arrive au niveau du Boss
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine((int)this.posX, (int)posY+8, "  ██████████▄      ▄██▀▀▀▀▀▀▀██▄       ▄█████████▀       ▄█████████▀", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+7, "  ██       ██      ███       ███       ██▀               ██         ", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+6, "  ██       ██      ███       ███       ██                ██         ", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+5, "  ██       █▀      ███       ███       ██▄▄▄▄▄▄▄▄        ██▄▄▄▄▄▄▄▄ ", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+4, "  ██▀▀▀▀▀▀▀█▄      ███       ███        ▀▀▀▀▀▀▀▀██        ▀▀▀▀▀▀▀▀██", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+3, "  ██       ██      ███       ███                ██                ██", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+2, "  ██       ██      ███       ███                ██                ██", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+1, "  ██       ██      ███       ███               ▄██               ▄██", Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+0, "  ██████████▀      ▀██▄▄▄▄▄▄▄██▀       ▄█████████▀       ▄█████████▀", Color.RED);
        return chaines;
    }
}
