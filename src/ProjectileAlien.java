import javafx.scene.paint.Color;

public class ProjectileAlien {
    
    private double posX;
    private double posY;

    /*
     * construit un nouveau ProjectileAlien
     * 
     * @param double posX
     * @param double posY
     */
    public ProjectileAlien(double posX, double posY){
        this.posX = posX;
        this.posY = posY;
    }

    /*
     * retourne la position X du projectile de alien
     * 
     * @return double
     */
    public double getX(){
        return this.posX;
    }

    /*
     * retourne la position Y du projectile de alien
     * 
     * @return double
     */
    public double getY(){
        return this.posY;
    }

    /*
     * change la position Y du projectile afin de le faire bouger
     */
    public void evolue(){
        this.posY -= 0.2;
    }

    /*
     * retourne un EnsembleChaines contenant le projectile de l'alien
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaine = new EnsembleChaines();
        chaine.ajouteChaine((int)this.posX, (int)this.posY, "♥", Color.RED);
        return chaine;
    }
}
