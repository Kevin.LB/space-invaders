import javafx.scene.paint.Color;

public class AlienV4 {
    
    private double posX;
    private double posY;
    private int tour;

    /*
     * Construit un nouvel AlienV4
     * 
     * @param double posX
     * @param double posY
     * @param int tour
     */
    public AlienV4(double posX, double posY, int tour) {
        this.posX = posX;
        this.posY = posY;
        this.tour = tour;
    }

    /*
     * sert à déplacer l'alien
     */
    public void evolue(){
        if(this.tour < 25){
            this.posX -= 0.4;
        }
        else if(this.tour == 25){
            this.posY -= 1;
        }
        else if(this.tour < 50){
            this.posX += 0.4;
        }
        else{
            this.tour = 0;
            this.posY -= 1;
        }
        this.tour ++;
    }

    /*
     * retourne la position Y de l'alien
     * 
     * @return double
     */
    public double getY(){
        return this.posY;
    }

    /*
     * retourne true si les coordonnées misent en paramêtre sont dans l'EnsembleChaines de l'alien sinon false
     * 
     * @param int x
     * @param int y
     * 
     * @return boolean
     */
    public boolean contient(int x, int y){
        for(int i = 0; 17>i; i++){
            if(((int)this.posX) + i == x){
                for(int j = 0; 4>j; j++){
                    if(((int)this.posY) + j == y){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /*
     * retourne la position du canon de l'alien
     * 
     * @return double
     */
    public double positionCanon(){
        return this.posX+8;
    }

    /*
     * retourne la position Y du canon de l'alien
     * 
     * @return double
     */
    public double getYCanon(){
        return this.posY;
    }

    /*
     * retourne un EnsembleChaines contenant l'alien
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        boolean etat;

        if(this.tour < 5){
            etat = true;
        }else if(this.tour < 10){
            etat = false;
        }else if(this.tour < 15){
            etat = true;
        }else if(this.tour < 20){
            etat = false;
        }else if(this.tour < 25){
            etat = true;
        }else if(this.tour < 30){
            etat = false;
        }else if(this.tour < 35){
            etat = true;
        }else if(this.tour < 40){
            etat = false;
        }else if(this.tour < 45){
            etat = true;
        }else {
            etat = false;
        }

        if(etat == true){
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "    ▄▄████▄▄     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "   ██████████    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "   ██▄▄██▄▄██    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "    ▄▀▄██▄▀▄     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "   ▀   ▀▀   ▀    ", Color.YELLOWGREEN);
            return chaines;
        }
        else{
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "    ▄▄████▄▄     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "   ██████████    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "   ██▄▄██▄▄██    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "  ▀▄▄▀▄██▄▀▄▄▀   ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "       ▀▀        ", Color.YELLOWGREEN);
            return chaines;
        }
        
    }
}
