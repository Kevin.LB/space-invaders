import javafx.scene.paint.Color;

public class GameOver {

    private double posX;
    private double posY;
    private boolean gameOver = false;

    /*
     * construit un nouvel objet GameOver
     * 
     * @param double posX
     * @param double posY
     */
    public GameOver(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    /*
     * retourne la valeur de la variable gameOver
     * 
     * @return boolean
     */
    public boolean getGameOver(){
        return this.gameOver;
    }

    /*
     * change la valeur de la variable gameOver en true et la retourne
     * 
     * @return boolean
     */
    public boolean setGameOver(){
        return this.gameOver = true;
    }

    /*
     * retourne un EnsembleChaines contenant le message affiché lorsque l'utisateur perd la partie
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+22, "███▀▀▀██  ███▀▀▀███  ███▀█▄█▀███  ██▀▀▀", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+21, "██    ██  ██     ██  ██   █   ██  ██   ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+20, "██   ▄▄▄  ██▄▄▄▄▄██  ██       ██  ██▀▀▀", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+19, "██    ██  ██     ██  ██       ██  ██   ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+18, "███▄▄▄██  ██     ██  ██       ██  ██▄▄▄", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+17, "                                       ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+16, "███▀▀▀███  ▀███  ██▀  ██▀▀▀  ██▀▀▀▀██▄ ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+15, "██     ██    ██  ██   ██     ██     ██ ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+14, "██     ██    ██  ██   ██▀▀▀  ██▄▄▄▄▄▀▀ ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+13, "██     ██    ██  █▀   ██     ██     ██ ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+12, "███▄▄▄███     ▀█▀     ██▄▄▄  ██     ██▄", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+11, "                                       ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+10, "          ██               ██          ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+9, "        ████▄   ▄▄▄▄▄▄▄   ▄████        ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+8, "           ▀▀█▄█████████▄█▀▀           ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+7, "             █████████████             ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+6, "             ██▀▀▀███▀▀▀██             ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+5, "             ██   ███   ██             ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "             █████▀▄▀█████             ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "              ███████████              ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "          ▄▄▄██  █▀█▀█  ██▄▄▄          ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "          ▀▀██           ██▀▀          ", Color.RED);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "            ▀▀           ▀▀            ", Color.RED);
            return chaines;
        }
}
