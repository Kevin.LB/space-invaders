import javafx.scene.paint.Color;

public class Win {

    private double posX;
    private double posY;
    private int score;

    /*
     * construit un nouvel objet Win
     * 
     * @param double posX
     * @param double posY
     */
    public Win(double posX, double posY, int score) {
        this.posX = posX;
        this.posY = posY;
        this.score = score;
    }

    /*
     * retourne le score
     * 
     * @return int
     */
    public int getScore(){
        return this.score;
    }

    /*
     * retourne un EnsembleChaines contenant le message de fin lorsque l'utilisateur gagne
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+35, "███▀▀▀██  ███▀▀▀███  ███▀▀▀██  ███▄    ██  ██▀▀▀  ██▀▀▀▀██▄   ██", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+34, "██    ██  ██     ██  ██    ██  ██ ██   ██  ██     ██     ██   ██", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+33, "██   ▄▄▄  ██▄▄▄▄▄██  ██   ▄▄▄  ██  ██  ██  ██▀▀▀  ██▄▄▄▄▄▀▀   ██", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+32, "██    ██  ██     ██  ██    ██  ██   ██ ██  ██     ██     ██   ██", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+31, "███▄▄▄██  ██     ██  ███▄▄▄██  ██    ▀███  ██▄▄▄  ██     ██▄  ▄▄", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+30, "                                                                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+29, "                                                                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+28, "                        SCORE FINAL : "+getScore()+"                   ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+27, "                                                                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+26, "                                                                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+25, "                       ▒▒▒▒      ▒▒▒▒▒▒▒▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+24, "                      ▒▒    ▒▒▒▒▒▒▒    ▒▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+23, "                    ▒▒      ▒▒▒▒▒      ▒▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+22, "                   ▒▒      ▒▒▒▒▒▒▒     ▒▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+21, "                   ▒▒    ▒▒▒▒▒▒▒▒▒▒▒  ▒                         ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+20, "                    ▒  ▒▒▒▒▒▒█▒▒▒▒▒█▒▒                          ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+19, "                      ▒▒▒▒▒▒ ██▒▒▒▒██ ▒   ▒▒   ▒▒               ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+18, "                      ▒▒▒▒▒    ███   ▒▒   ▒▒  ▒▒                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+17, "                      ▒▒▒▒▒     ███    ▒▒  ▒▒ ▒▒                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+16, "                       ▒▒    ▀▄▄▄▄▄▄▀ ▒    ▒▒▒▒▒                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+15, "                        ▒▒▒      ▀▀  ▒▒   ▒▒▒▒▒▒                ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+14, "                      ▒▒▒ ▒   ▒▒▒  ▒▒▒▒▒▒▒▒▒▒▒                  ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+13, "                     ▒▒▒ ▒▒▒▒▒   ▒ ▒▒▒▒▒▒▒▒▒                    ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+12, "                    ▒▒▒ ▒▒▒▒    ▒▒  ▒▒▒▒▒▒▒▒▒                   ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+11, "                    ▒▒▒ ▒               ▒▒▒▒                    ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+10, "                    ▒▒▒ ▒  ▒            ▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+9, "                    ▒▒▒▒▒  ▒▒     ▒ ▓▓▓ ▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+8, "                    ▒▒▒▒▒  ▒▒▒  ▒▒ ▓▓▓▓ ▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+7, "                   ▒▒▒▒▒ ▒▒▒▒▒▒▒▒ ▓▓▓▓▓ ▒                       ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+6, "                   ▒▒▒   ▒▒▒▒▒▒▒▒▒ ▓▓▓▓▓ ▒                      ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+5, "                    ▒▒   ▒▒▒    ▒▒▒ ▓▓▓ ▒▒                      ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "                     ▒▒  ▒▒  ▓▓▓ ▒_▒▒▒▒▒▒▒                      ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "                      ▒▒▒▒▒ ▓▓▓▓ ▒                              ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "                          ▒▒ ▓▓▓▓ ▒                             ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "                           ▒▒ ▓▓▓ ▒                             ", Color.YELLOW);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "                             ▒▒▒▒▒▒                             ", Color.YELLOW);
            return chaines;
        }
}





