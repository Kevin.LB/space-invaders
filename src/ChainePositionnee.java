import javafx.scene.paint.Color;

public class ChainePositionnee{
    int x,y;
    String c;
    Color color;

    /*
     * construit une nouvelle ChainePositionnee
     * 
     * @param int a
     * @param int b
     * @param String d
     */
    public ChainePositionnee(int a,int b, String d){x=a; y=b; c=d;color = Color.BEIGE; }

    /*
     * construit une nouvelle ChainePositionnee avec de la couleur 
     * 
     * @param int a
     * @param int b
     * @param String d
     * @param Color color
     */
    public ChainePositionnee(int a,int b, String d, Color couleur){x=a; y=b; c=d; color = couleur;}
}
