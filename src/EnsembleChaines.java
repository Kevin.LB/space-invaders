import java.util.ArrayList;
import javafx.scene.paint.Color;

public class EnsembleChaines {
    ArrayList<ChainePositionnee> chaines;

    /*
     * construit un nouveau EnsembleChaines
     * 
     * @param int a
     * @param int b
     * @param String d
     */
    public EnsembleChaines(){chaines= new ArrayList<ChainePositionnee>(); }

    /*
     * ajoute une nouvelle ChainePositionnee à l'EnsembleChaines
     * 
     * @param int a
     * @param int b
     * @param String c
     */
    public void ajouteChaine(int x, int y, String c){
        chaines.add(new ChainePositionnee(x,y,c));}

    /*
     * ajoute une nouvelle ChainePositionnee avec de la couleur à l'EnsembleChaines
     * 
     * @param int a
     * @param int b
     * @param String c
     * @param Color color
     */
    public void ajouteChaine(int x, int y, String c, Color color){
        chaines.add(new ChainePositionnee(x,y,c, color));}

    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }

    /*
     * retourne true si une ChainePositionne de l'EnsembleChaine contient les coordonnées mis en paramêtre
     * 
     * @return boolean
     */
    public boolean contient(int x, int y){
        for(ChainePositionnee chaines : this.chaines){
            if(chaines.x == x && chaines.y == y){
                return true;
            }
        }
        return false;
    }
}