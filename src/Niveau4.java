import javafx.scene.paint.Color;

public class Niveau4 {
    private double posX;
    private double posY;

    /*
     * construit un nouvel objet Niveau4
     * 
     * @param double posX
     * @param double posY
     */
    public Niveau4(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    /*
     * retourne un EnsembleChaines contenant le message affiché lorsque l'utisateur arrive au niveau 4
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine((int)this.posX, (int)posY+14, " ██▄     ██  ██  ▄██        ██▄  ██▀▀▀▀  ███▀▀▀███   ██      ██ ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+13, " ██ ██   ██  ██    ██      ██    ██      ██     ██   ██      ██ ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+12, " ██  ██  ██  ██     ██    ██     ██▀▀▀▀  ██▄▄▄▄▄██   ██      ██ ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+11, " ██   ██ ██  ██      ██  ██      ██      ██     ██   ▀█▄    ▄█▀ ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+10, " ██     ▀██  ██       ▀██▀       ██▄▄▄▄  ██     ██     ▀████▀   ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+9, "                                                                ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+8, "                                                                ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+7, "                                 ▄██                            ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+6, "                               ▄████                            ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+5, "                             ▄██  ██                            ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+4, "                           ▄██    ██                            ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+3, "                         ▄██      ██                            ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+2, "                         ██████████████▄                        ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+1, "                                  ██                            ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+0, "                                  ██                            ", Color.YELLOWGREEN);
        return chaines;
    }
}
