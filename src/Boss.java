import javafx.scene.paint.Color;

public class Boss {
    
    private double posX;
    private double posY;
    private int tour;
    private int vie = 5;

    /*
     * Construit un nouveau Boss
     * 
     * @param double posX
     * @param double posY
     * @param int tour
     */
    public Boss(double posX, double posY, int tour) {
        this.posX = posX;
        this.posY = posY;
        this.tour = tour;
    }

    /*
     * sert à déplacer le boss
     */
    public void evolue(){
        if(this.tour < 100){
            this.posX -= 0.5;
        }
        else if(this.tour == 100){
            this.posY -= 2;
        }
        else if(this.tour < 200){
            this.posX += 0.5;
        }
        else{
            this.tour = 0;
            this.posY -= 2;
        }
        this.tour ++;
    }

    /*
     * retourne la position Y du boss
     * 
     * @return double
     */
    public double getY(){
        return this.posY;
    }

    /*
     * retourne le nombre de vie du boss
     * 
     * @return int
     */
    public int getVie(){
        return this.vie;
    }

    /*
     * change la valeur du nombre de vie du boss
     * 
     * @param int points
     */
    public void perd(int points){
        this.vie -= points;
    }

    /*
     * retourne true si les coordonnées misent en paramêtre sont dans l'EnsembleChaines du boss sinon false
     * 
     * @param int x
     * @param int y
     * 
     * @return boolean
     */
    public boolean contient(int x, int y){
        for(int i = 0; 23>i; i++){
            if(((int)this.posX) + i == x){
                for(int j = 0; 4>j; j++){
                    if(((int)this.posY) + j == y){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /*
     * retourne la position du premier canon du boss
     * 
     * @return double
     */
    public double positionCanon1(){
        return this.posX+4;
    }

    /*
     * retourne la position du deuxième canon du boss
     * 
     * @return double
     */
    public double positionCanon2(){
        return this.posX+9;
    }

    /*
     * retourne la position du troisième canon du boss
     * 
     * @return double
     */
    public double positionCanon3(){
        return this.posX+15;
    }

    /*
     * retourne la position du quatrième canon du boss
     * 
     * @return double
     */
    public double positionCanon4(){
        return this.posX+20;
    }

    /*
     * retourne la position Y du canon du boss
     * 
     * @return double
     */
    public double getYCanon(){
        return this.posY;
    }

    /*
     * retourne un EnsembleChaines contenant le boss
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine((int)this.posX, (int)posY+7, ("        Vie : "+getVie()+"         "), Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+6, ("                        "), Color.RED);
        chaines.ajouteChaine((int)this.posX, (int)posY+5, "       ▄▄█████▄▄       ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+4, "     ▄███████████▄     ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+3, "    ██▀▀██▀▀██▀▀███    ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+2, "  ▄███▄▄██▄▄██▄▄████▄  ", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+1, "▄█████████████████████▄", Color.YELLOWGREEN);
        chaines.ajouteChaine((int)this.posX, (int)posY+0, "  ▀█▀  ▀█▀   ▀█▀  ▀█▀  ", Color.RED);
        return chaines;
    }
}
