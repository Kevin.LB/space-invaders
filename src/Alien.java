import javafx.scene.paint.Color;

public class Alien {
    
    private double posX;
    private double posY;
    private int tour;

    /*
     * Construit un nouvel Alien
     * 
     * @param double posX
     * @param double posY
     * @param int tour
     */
    public Alien(double posX, double posY, int tour) {
        this.posX = posX;
        this.posY = posY;
        this.tour = tour;
    }

    /*
     * retourne la position Y de l'alien
     * 
     * @return double
     */
    public double getY(){
        return this.posY;
    }

    /*
     * sert à déplacer l'alien
     */
    public void evolue(){
        if(this.tour < 100){
            this.posX -= 0.1;
        }
        else if(this.tour == 100){
            this.posY -= 1;
        }
        else if(this.tour < 200){
            this.posX += 0.1;
        }
        else{
            this.tour = 0;
            this.posY -= 1;
        }
        this.tour ++;
    }

    /*
     * retourne true si les coordonnées misent en paramêtre sont dans l'EnsembleChaines de l'alien sinon false
     * 
     * @param int x
     * @param int y
     * 
     * @return boolean
     */
    public boolean contient(int x, int y){
        for(int i = 0; 17>i; i++){
            if(((int)this.posX) + i == x){
                for(int j = 0; 4>j; j++){
                    if(((int)this.posY) + j == y){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /*
     * retourne un EnsembleChaines contenant l'alien
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        boolean etat;

        if(this.tour < 25){
            etat = true;
        }else if(this.tour < 50){
            etat = false;
        }else if(this.tour < 75){
            etat = true;
        }else if(this.tour < 100){
            etat = false;
        }else if(this.tour < 125){
            etat = true;
        }else if(this.tour < 150){
            etat = false;
        }else if(this.tour < 175){
            etat = true;
        }else {
            etat = false;
        }

        if(etat == true){
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "     ▀▄   ▄▀     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "    ▄█▀███▀█▄    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "   █▀███████▀█   ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "   █ █▀▀▀▀▀█ █   ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "      ▀▀ ▀▀      ", Color.YELLOWGREEN);
            return chaines;
        }
        else{
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "     ▀▄   ▄▀     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "  █  ▄▀███▀▄  █  ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "   █▄███████▄█   ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "     █▀▀▀▀▀█     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "      ▀▀ ▀▀      ", Color.YELLOWGREEN);
            return chaines;
        }
        
    }
}