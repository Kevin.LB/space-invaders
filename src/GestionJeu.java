import java.util.ArrayList;
import java.util.List;

public class GestionJeu{

    private Vaisseau vaisseau;
    private EnsembleChaines chaines;
    private List<Projectile> projectiles;
    private List<Projectile> projectilesToucher;
    private List<ProjectileAlien> projectilesAlien;
    private List<ProjectileAlien> projectilesToucherAlien;
    private int vitesse = 1;
    private Score score;
    private List<Alien> aliens;
    private List<AlienV2> aliensV2;
    private List<AlienV3> aliensV3;
    private List<AlienV4> aliensV4;
    private List<Boss> aliensV5;
    private List<Alien> aliensToucher;
    private List<AlienV2> aliensToucherV2;
    private List<AlienV3> aliensToucherV3;
    private List<AlienV4> aliensToucherV4;
    private List<Boss> aliensToucherV5;
    private int cptPoint = 0;
    private int cptWin = 0;
    private int cptNiveau1 = 0;
    private int cptNiveau2 = 0;
    private int cptNiveau3 = 0;
    private int cptNiveau4 = 0;
    private int cptNiveau5 = 0;
    private int cptTireLvl3 = 0;
    private int cptTireLvl4 = 0;
    private int cptTireLvl5 = 0;
    private GameOver gameOver;
    private Win win;
    private Niveau1 affichageNiveau1;
    private Niveau2 affichageNiveau2;
    private Niveau3 affichageNiveau3;
    private Niveau4 affichageNiveau4;
    private Niveau5 affichageNiveau5;

    /*
     * Construit un nouvel objet GestionJeu
     */
    public GestionJeu() {
        this.vaisseau = new Vaisseau(45);
        this.chaines = new EnsembleChaines();
        this.projectiles = new ArrayList<>();
        this.projectilesAlien = new ArrayList<>();
        this.score = new Score();
        this.aliens = new ArrayList<>();
        this.aliensToucher = new ArrayList<>();
        this.aliensToucherV2 = new ArrayList<>();
        this.aliensToucherV3 = new ArrayList<>();
        this.aliensToucherV4 = new ArrayList<>();
        this.aliensToucherV5 = new ArrayList<>();
        this.projectilesToucher = new ArrayList<>();
        this.projectilesToucherAlien = new ArrayList<>();
        aliens.add(new Alien(82, 44, 0));
        aliens.add(new Alien(82, 37, 0));
        aliens.add(new Alien(82, 30, 0));
        aliens.add(new Alien(82, 23, 0));
        aliens.add(new Alien(66, 44, 0));
        aliens.add(new Alien(66, 37, 0));
        aliens.add(new Alien(66, 30, 0));
        aliens.add(new Alien(66, 23, 0));
        aliens.add(new Alien(50, 44, 0));
        aliens.add(new Alien(50, 37, 0));
        aliens.add(new Alien(50, 30, 0));
        aliens.add(new Alien(50, 23, 0));
        aliens.add(new Alien(34, 44, 0));
        aliens.add(new Alien(34, 37, 0));
        aliens.add(new Alien(34, 23, 0));
        aliens.add(new Alien(34, 30, 0));
        aliens.add(new Alien(18, 44, 0));
        aliens.add(new Alien(18, 37, 0));
        aliens.add(new Alien(18, 30, 0));
        aliens.add(new Alien(18, 23, 0));
        this.aliensV2 = new ArrayList<>();
        aliensV2.add(new AlienV2(82, 44, 0));
        aliensV2.add(new AlienV2(82, 30, 0));
        aliensV2.add(new AlienV2(66, 37, 0));
        aliensV2.add(new AlienV2(66, 23, 0));
        aliensV2.add(new AlienV2(50, 44, 0));
        aliensV2.add(new AlienV2(50, 30, 0));
        aliensV2.add(new AlienV2(34, 37, 0));
        aliensV2.add(new AlienV2(34, 23, 0));
        aliensV2.add(new AlienV2(18, 44, 0));
        aliensV2.add(new AlienV2(18, 30, 0));
        this.aliensV3 = new ArrayList<>();
        aliensV3.add(new AlienV3(82, 37, 0));
        aliensV3.add(new AlienV3(66, 44, 0));
        aliensV3.add(new AlienV3(50, 37, 0));
        aliensV3.add(new AlienV3(34, 44, 0));
        aliensV3.add(new AlienV3(18, 37, 0));
        this.aliensV4 = new ArrayList<>();
        aliensV4.add(new AlienV4(82, 44, 0));
        aliensV4.add(new AlienV4(66, 37, 0));
        aliensV4.add(new AlienV4(50, 44, 0));
        aliensV4.add(new AlienV4(34, 37, 0));
        aliensV4.add(new AlienV4(18, 44, 0));
        this.aliensV5 = new ArrayList<>();
        aliensV5.add(new Boss(60, 44, 0));
        this.gameOver = new GameOver(30, 20);
        this.win = new Win(18, 13, 0);
        this.affichageNiveau1 = new Niveau1(18, 22);
        this.affichageNiveau2 = new Niveau2(18, 22);
        this.affichageNiveau3 = new Niveau3(18, 22);
        this.affichageNiveau4 = new Niveau4(18, 22);
        this.affichageNiveau5 = new Niveau5(18, 28);
    }

    /*
     * retourne la hauteur du jeu
     * 
     * @return int
     */
    public int getHauteur() {
        return 60;
    }

    /*
     * retourne la largeur du jeu
     * 
     * @return int
     */
    public int getLargeur() {
        return 100;
    }

    /*
     * permet de déplacer le vaisseau vers la gauche
     */
    public void toucheGauche() {
        if(this.vaisseau.getPosX() > 1){
            this.chaines = new EnsembleChaines();
            this.vaisseau.deplace(-vitesse);
            this.chaines.union(this.vaisseau.getEnsembleChaines());
        } 
    }

    /*
     * permet de déplacer le vaisseau vers la droite
     */
    public void toucheDroite() {
        if(this.vaisseau.getPosX() < 86){
            this.chaines = new EnsembleChaines();
            this.vaisseau.deplace(vitesse);
            this.chaines.union(this.vaisseau.getEnsembleChaines());
        } 
    }

    /*
     * permet au vaisseau de tirer un projectile
     */
    public void toucheEspace() {
        if(projectiles.size() < 3){
            Projectile projectile = new Projectile(this.vaisseau.positionCanon(), 5);
            this.projectiles.add(projectile);
        }
    }

    /*
     * retourne l'EnsembleChaines du jeu
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getChaines() {
        return chaines;
    }

    /*
     * permet de jouer un tour du jeu
     */
    public void jouerUnTour() {
        this.chaines = new EnsembleChaines();
        this.chaines.union(this.vaisseau.getEnsembleChaines());
        this.chaines.union(this.score.getEnsembleChaines());

        if(projectilesToucherAlien.size() == 3){
            this.gameOver.setGameOver();
        }
        if(this.score.getVie() <= 0){
            this.gameOver.setGameOver();
        }

        if(!this.gameOver.getGameOver()){
            cptPoint ++;
            if(cptPoint == 100 ){
                this.score.ajoute(10);
                cptPoint = 0;
            }
        }
            

        // AFFICHAGE NIVEAU 1

        if(!this.gameOver.getGameOver()){
            this.cptNiveau1 ++;
            if(cptNiveau1 < 75){
                this.chaines = new EnsembleChaines();
                this.chaines.union(this.affichageNiveau1.getEnsembleChaines());
            }

            // NIVEAU 1
            
            else if(aliens.size() > 0){
                try {
                    for(Projectile projectile : projectiles){
                        if((int)projectile.getY() <= this.getHauteur()){
                            projectile.evolue();
                            this.chaines.union(projectile.getEnsembleChaines());
                        }
                        else{
                            projectiles.remove(projectile);
                        }
                    }

                    for(Projectile b : projectiles){
                        for(Alien a : aliens){
                            if(aliens.size() == 1){
                                if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                    this.aliensToucher.add(a);
                                    this.aliens.remove(a);
                                    this.projectilesToucher.add(b);
                                    this.projectiles = new ArrayList<>();
                                    this.score.ajoute(50);
                                } 
                            }
                            if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                this.aliensToucher.add(a);
                                this.aliens.remove(a);
                                this.projectilesToucher.add(b);
                                this.projectiles.remove(b);
                                this.score.ajoute(50);
                            } 
                            
                        }
                    }
    
                    for(Alien alien : aliens){
                        alien.evolue();
                        this.chaines.union(alien.getEnsembleChaines());
                        if(alien.getY() < 6){
                            this.score.perd(3);
                        }
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }
            }



            // AFFICHAGE NIVEAU 2

            else if(aliens.size() == 0 && cptNiveau2 < 75){
                this.cptNiveau2 ++;
                this.chaines = new EnsembleChaines();
                this.chaines.union(this.affichageNiveau2.getEnsembleChaines());
            }

            // NIVEAU 2

            else if(cptNiveau2 == 75 && aliensV2.size() > 0){
                try {
                    for(Projectile projectile : projectiles){
                        if((int)projectile.getY() <= this.getHauteur()){
                            projectile.evolue();
                            this.chaines.union(projectile.getEnsembleChaines());
                        }
                        else{
                            projectiles.remove(projectile);
                        }
                    }
        
                    for(Projectile b : projectiles){
                        for(AlienV2 a : aliensV2){
                            if(aliensV2.size() == 1){
                                if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                    this.aliensToucherV2.add(a);
                                    this.aliensV2.remove(a);
                                    this.projectilesToucher.add(b);
                                    this.projectiles = new ArrayList<>();
                                    this.projectilesAlien = new ArrayList<>();
                                    this.score.ajoute(100);
                                } 
                            }
                            if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                this.aliensToucherV2.add(a);
                                this.aliensV2.remove(a);
                                this.projectilesToucher.add(b);
                                this.projectiles.remove(b);
                                this.score.ajoute(50);
                            } 
                        }
                    }
                    
        
                    for(AlienV2 alienV2 : aliensV2){
                        alienV2.evolue();
                        this.chaines.union(alienV2.getEnsembleChaines());
                        if(alienV2.getY() < 6){
                            this.score.perd(3);
                        }
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }
            }
            



            // AFFICHAGE NIVEAU 3

            else if(aliensV2.size() == 0 && cptNiveau3 < 75){
                this.cptNiveau3 ++;
                this.chaines = new EnsembleChaines();
                this.chaines.union(this.affichageNiveau3.getEnsembleChaines());
            }

            // NIVEAU 3

            else if(cptNiveau3 == 75 && aliensV3.size() > 0){
                try{
                    this.cptTireLvl3 ++;
                    for(AlienV3 alienV3 : aliensV3){
                        if(this.cptTireLvl3 == 75){
                            ProjectileAlien projectile = new ProjectileAlien(alienV3.positionCanon(), alienV3.getYCanon());
                            this.projectilesAlien.add(projectile);
                        }
                        alienV3.evolue();
                        this.chaines.union(alienV3.getEnsembleChaines());
                        if(alienV3.getY() < 6){
                            this.score.perd(3);
                        }
                    }
                    if(this.cptTireLvl3 == 75){
                        this.cptTireLvl3 = 0;
                    }

                    for(ProjectileAlien b : projectilesAlien){
                        if(this.vaisseau.contient((int)b.getX(),(int)b.getY()) == true){
                            this.projectilesToucherAlien.add(b);
                            this.projectilesAlien.remove(b);
                            this.score.ajoute(-200);
                            this.score.perd(1);
                        } 
                        if((int)b.getY() >= 0){
                            b.evolue();
                            this.chaines.union(b.getEnsembleChaines());
                        }
                        else{
                            projectilesAlien.remove(b);
                        }
                        
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }

                
                
                try {
                    for(Projectile projectile : projectiles){
                        if((int)projectile.getY() <= this.getHauteur()){
                            projectile.evolue();
                            this.chaines.union(projectile.getEnsembleChaines());
                        }
                        else{
                            projectiles.remove(projectile);
                        }
                    }
        
                    for(Projectile b : projectiles){
                        for(AlienV3 a : aliensV3){
                            if(aliensV3.size() == 1){
                                if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                    this.aliensToucherV3.add(a);
                                    this.aliensV3.remove(a);
                                    this.projectilesToucher.add(b);
                                    this.projectiles = new ArrayList<>();
                                    this.projectilesAlien = new ArrayList<>();
                                    this.score.ajoute(200);
                                } 
                            }
                            if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                this.aliensToucherV3.add(a);
                                this.aliensV3.remove(a);
                                this.projectilesToucher.add(b);
                                this.projectiles.remove(b);
                                this.score.ajoute(50);
                            } 
                        }
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }
                    
            }

            // AFFICHAGE NIVEAU 4

            else if(aliensV3.size() == 0 && cptNiveau4 < 75){
                this.cptNiveau4 ++;
                this.chaines = new EnsembleChaines();
                this.chaines.union(this.affichageNiveau4.getEnsembleChaines());
            }

            // NIVEAU 4

            else if(cptNiveau4 == 75 && aliensV4.size() > 0){
                try{
                    this.cptTireLvl4 ++;
                    for(AlienV4 alienV4 : aliensV4){
                        if(this.cptTireLvl4 == 65){
                            ProjectileAlien projectile = new ProjectileAlien(alienV4.positionCanon(), alienV4.getYCanon());
                            this.projectilesAlien.add(projectile);
                        }
                        alienV4.evolue();
                        this.chaines.union(alienV4.getEnsembleChaines());
                        if(alienV4.getY() < 6){
                            this.score.perd(3);
                        }
                    }
                    if(this.cptTireLvl4 == 65){
                        this.cptTireLvl4 = 0;
                    }

                    for(ProjectileAlien b : projectilesAlien){
                        if(this.vaisseau.contient((int)b.getX(),(int)b.getY()) == true){
                            this.projectilesToucherAlien.add(b);
                            this.projectilesAlien.remove(b);
                            this.score.ajoute(-1000);
                            this.score.perd(1);
                        } 
                        if((int)b.getY() >= 0){
                            b.evolue();
                            this.chaines.union(b.getEnsembleChaines());
                        }
                        else{
                            projectilesAlien.remove(b);
                        }
                        
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }


                try {
                    for(Projectile projectile : projectiles){
                        if((int)projectile.getY() <= this.getHauteur()){
                            projectile.evolue();
                            this.chaines.union(projectile.getEnsembleChaines());
                        }
                        else{
                            projectiles.remove(projectile);
                        }
                    }
        
                    for(Projectile b : projectiles){
                        for(AlienV4 a : aliensV4){
                            if(aliensV4.size() == 1){
                                if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                    this.aliensToucherV4.add(a);
                                    this.aliensV4.remove(a);
                                    this.projectilesToucher.add(b);
                                    this.projectiles = new ArrayList<>();
                                    this.projectilesAlien = new ArrayList<>();
                                    this.score.ajoute(350);
                                } 
                            }
                            if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                this.aliensToucherV4.add(a);
                                this.aliensV4.remove(a);
                                this.projectilesToucher.add(b);
                                this.projectiles.remove(b);
                                this.score.ajoute(50);
                            } 
                        }
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }
            }

            // AFFICHAGE NIVEAU 5

            else if(aliensV4.size() == 0 && cptNiveau5 < 75){
                this.cptNiveau5 ++;
                this.chaines = new EnsembleChaines();
                this.chaines.union(this.affichageNiveau5.getEnsembleChaines());
            }

            // NIVEAU 5

            else if(cptNiveau5 == 75 && aliensV5.size() > 0){
                try{
                    this.cptTireLvl5 ++;
                    for(Boss boss : aliensV5){
                        if(this.cptTireLvl5 == 15){
                            ProjectileAlien projectile = new ProjectileAlien(boss.positionCanon1(), boss.getYCanon());
                            ProjectileAlien projectile2 = new ProjectileAlien(boss.positionCanon2(), boss.getYCanon());
                            ProjectileAlien projectile3 = new ProjectileAlien(boss.positionCanon3(), boss.getYCanon());
                            ProjectileAlien projectile4 = new ProjectileAlien(boss.positionCanon4(), boss.getYCanon());
                            this.projectilesAlien.add(projectile);
                            this.projectilesAlien.add(projectile2);
                            this.projectilesAlien.add(projectile3);
                            this.projectilesAlien.add(projectile4);
                        }
                        boss.evolue();
                        this.chaines.union(boss.getEnsembleChaines());
                        if(boss.getY() < 6){
                            this.score.perd(3);
                        }
                    }
                    if(this.cptTireLvl5 == 15){
                        this.cptTireLvl5 = 0;
                    }

                    for(ProjectileAlien b : projectilesAlien){
                        if(this.vaisseau.contient((int)b.getX(),(int)b.getY()) == true){
                            this.projectilesToucherAlien.add(b);
                            this.projectilesAlien.remove(b);
                            this.score.ajoute(-1000);
                            this.score.perd(1);
                        } 
                        if((int)b.getY() >= 0){
                            b.evolue();
                            this.chaines.union(b.getEnsembleChaines());
                        }
                        else{
                            projectilesAlien.remove(b);
                        }
                        
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }


                try {
                    for(Projectile projectile : projectiles){
                        if((int)projectile.getY() <= this.getHauteur()){
                            projectile.evolue();
                            this.chaines.union(projectile.getEnsembleChaines());
                        }
                        else{
                            projectiles.remove(projectile);
                        }
                    }
        
                    for(Projectile b : projectiles){
                        for(Boss a : aliensV5){
                            if(a.contient((int)b.getX(),(int)b.getY()) == true){
                                a.perd(1);
                                if(a.getVie() == 0){
                                    this.aliensV5.remove(a);
                                    this.aliensToucherV5.add(a);  
                                    this.score.ajoute(500);
                                }
                                this.projectilesToucher.add(b);
                                this.projectiles.remove(b);
                            }
                        }
                    }
                }
                catch (Exception e){
                    this.chaines = new EnsembleChaines();
                    this.chaines.union(vaisseau.getEnsembleChaines());
                }
            }
                
            // AFFICHAGE WIN

            else {
                this.projectiles = new ArrayList<>();
                this.projectilesAlien = new ArrayList<>();
                int scoreFinal = 0;
                if(cptWin == 1){
                    this.score.ajoutePointsParVie();
                    scoreFinal = this.score.getScore();
                    this.win = new Win(18, 12, scoreFinal);
                }
                this.chaines = new EnsembleChaines();
                this.chaines.union(this.win.getEnsembleChaines());
                cptWin ++;
            }
        
        }

            // AFFICHAGE GAME OVER

        else {
            this.chaines = new EnsembleChaines();
            this.chaines.union(this.gameOver.getEnsembleChaines());
        }
    }

    
}
