public class Vaisseau {
    private double posX;
    
    /*
     * Construit un nouveau Vaisseau
     * 
     * @param double posX
     */
    public Vaisseau(double posX) {
        this.posX = posX;
    }
    
    /*
     * retourne la position X du vaisseau
     * 
     * @return double
     */
    public double getPosX(){
        return this.posX;
    }
    
    /*
     * sert à déplacer le vaiseau
     * 
     * @param double x
     */
    public void deplace(double dx) {
        this.posX += dx;
    }


    /*
     * retourne true si les coordonnées misent en paramêtre sont dans l'EnsembleChaines du vaisseau sinon false
     * 
     * @param int x
     * @param int y
     * 
     * @return boolean
     */
    public boolean contient(int x, int y){
        for(int i = 0; 13>i; i++){
            if(((int)this.posX) + i == x){
                for(int j = 0; 5>j; j++){
                    if( j == y){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /*
     * retourne un EnsembleChaines contenant le vaisseau
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine((int)this.posX, 5, "      ▄      ");
        chaines.ajouteChaine((int)this.posX, 4, "    ▄███▄    ");
        chaines.ajouteChaine((int)this.posX, 3, "   ▄█████▄   ");
        chaines.ajouteChaine((int)this.posX, 2, "▄███████████▄");
        chaines.ajouteChaine((int)this.posX, 1, "▀███████████▀");
        chaines.ajouteChaine((int)this.posX, 0, "  ◢ ◣   ◢ ◣  ");
        return chaines;
    }

    /*
     * retourne la position du canon du vaisseau
     * 
     * @return double
     */
    public double positionCanon(){
        return this.posX+6;
    }
}
