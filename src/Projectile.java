import javafx.scene.paint.Color;

public class Projectile {
    
    private double posX;
    private double posY;

    /*
     * construit un nouveau Projectile
     * 
     * @param double posX
     * @param double posY
     */
    public Projectile(double posX, double posY){
        this.posX = posX;
        this.posY = posY;
    }

    /*
     * retourne la position X du projectile du vaisseau
     * 
     * @return double
     */
    public double getX(){
        return this.posX;
    }

    /*
     * retourne la position Y du projectile du vaisseau
     * 
     * @return double
     */
    public double getY(){
        return this.posY;
    }

    /*
     * change la position Y du projectile afin de le faire bouger
     */
    public void evolue(){
        this.posY += 0.2;
    }

    /*
     * retourne un EnsembleChaines contenant le projectile du vaisseau
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaine = new EnsembleChaines();
        chaine.ajouteChaine((int)this.posX, (int)this.posY, "♥", Color.YELLOW);
        return chaine;
    }
}
