import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;




public class Executable extends Application {
    private Pane root;
    private Group caracteres;
    private GestionJeu gestionnaire;
    private int hauteurTexte;
    private int largeurCaractere;
    private Scene scene2;
    public static void main(String[] args) {
        launch(args);
    }

    private void afficherCaracteres(){
        caracteres.getChildren().clear();
        int hauteur = (int) root.getHeight();
        for( ChainePositionnee c : gestionnaire.getChaines().chaines)
        {
            Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            t.setFill(c.color);
            caracteres.getChildren().add(t);
        }
    }

    private void lancerAnimation() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            gestionnaire.jouerUnTour();
                            afficherCaracteres();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("IUTO Space Invader");
        caracteres = new Group();
        root= new AnchorPane(caracteres);
        gestionnaire = new GestionJeu();
        Text t=new Text("█");
        t.setFont(Font.font("Monospaced",10));
        hauteurTexte =(int) t.getLayoutBounds().getHeight();
        largeurCaractere = (int) t.getLayoutBounds().getWidth();
        
        Scene scene = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            if(key.getCode()==KeyCode.LEFT)
                gestionnaire.toucheGauche();
            if(key.getCode()==KeyCode.RIGHT)
                gestionnaire.toucheDroite();
            if(key.getCode()==KeyCode.SPACE)
                gestionnaire.toucheEspace();
            if(key.getCode()==KeyCode.ENTER){
                this.gestionnaire = new GestionJeu();
                primaryStage.setScene(scene2);
            }
        });

        Button button = new Button("Jouer");
        button.setOnAction(e -> {
            primaryStage.setScene(scene);
            this.gestionnaire = new GestionJeu();
        });
        
        Text text1 = new Text("Voici ma version du jeu Space Invaders");
        Text text2 = new Text("Pour faire avancer le vaisseau sur la gauche : appuyer sur la flèche de gauche ");
        Text text3 = new Text("Pour faire avancer le vaisseau sur la droite : appuyer sur la flèche de droite ");
        Text text4 = new Text("Pour tirer : appuyer sur la touche espace (tu peut tirer que 3 projectiles à la fois)");
        Text text5 = new Text("Pour relancer le jeu une fois mort ou après avoir gagner : appuyer sur la touche entrée ");
        Text text13 = new Text("");
        Text text6 = new Text("Ce jeu se compose de 5 niveaux :");
        Text text7 = new Text("1er niveau : échauffement");
        Text text8 = new Text("2ème niveau : facile");
        Text text9 = new Text("3ème niveau : moyen");
        Text text10 = new Text("4ème niveau : difficile");
        Text text15 = new Text("5ème niveau : BOSS");
        Text text14 = new Text("");
        Text text16 = new Text("Record de points à battre : 5820");
        Text text17 = new Text("");
        Text text11 = new Text("Cliquez sur le bouton pour jouer au jeu !");
        Text text12 = new Text("Amusez-vous bien !");

        BackgroundFill backgroundFill = new BackgroundFill(Color.YELLOWGREEN, new CornerRadii(5), Insets.EMPTY);
        Background background1 = new Background(backgroundFill);
        button.setBackground(background1);

        BackgroundFill backgroundFill2 = new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(5), Insets.EMPTY);
        Background background2 = new Background(backgroundFill2);

        VBox layout= new VBox();
        layout.getChildren().addAll(text1, text2, text3, text4, text5, text13, text6, text7, text8, text9, text10, text15, text14, text16, text17, text11, text12, button);
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(20);
        layout.setBackground(background2);

        this.scene2 = new Scene(layout, gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
        
        Image image = new Image("file:img/bg.jpeg");
        BackgroundImage backgroundImage = new BackgroundImage(image,BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT,BackgroundPosition.CENTER,BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        root.setBackground(background);

        
        primaryStage.setScene(scene2);
        primaryStage.setResizable(false);
        primaryStage.show();
        lancerAnimation();
    }

}
