import javafx.scene.paint.Color;

public class AlienV2 {
    
    private double posX;
    private double posY;
    private int tour;

    /*
     * Construit un nouvel AlienV2
     * 
     * @param double posX
     * @param double posY
     * @param int tour
     */
    public AlienV2(double posX, double posY, int tour) {
        this.posX = posX;
        this.posY = posY;
        this.tour = tour;
    }

    /*
     * retourne la position Y de l'alien
     * 
     * @return double
     */
    public double getY(){
        return this.posY;
    }

    /*
     * sert à déplacer l'alien
     */
    public void evolue(){
        if(this.tour < 40){
            this.posX -= 0.25;
        }
        else if(this.tour == 40){
            this.posY -= 1;
        }
        else if(this.tour < 80){
            this.posX += 0.25;
        }
        else{
            this.tour = 0;
            this.posY -= 1;
        }
        this.tour ++;
    }

    /*
     * retourne true si les coordonnées misent en paramêtre sont dans l'EnsembleChaines de l'alien sinon false
     * 
     * @param int x
     * @param int y
     * 
     * @return boolean
     */
    public boolean contient(int x, int y){
        for(int i = 0; 17>i; i++){
            if(((int)this.posX) + i == x){
                for(int j = 0; 4>j; j++){
                    if(((int)this.posY) + j == y){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /*
     * retourne un EnsembleChaines contenant l'alien
     * 
     * @return EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines(){
        boolean etat;

        if(this.tour < 10){
            etat = true;
        }else if(this.tour < 20){
            etat = false;
        }else if(this.tour < 30){
            etat = true;
        }else if(this.tour < 40){
            etat = false;
        }else if(this.tour < 50){
            etat = true;
        }else if(this.tour < 60){
            etat = false;
        }else if(this.tour < 70){
            etat = true;
        }else {
            etat = false;
        }

        if(etat == true){
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "      ▄██▄       ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "    ▄██████▄     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "   ███▄██▄███    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "     ▄▀▄▄▀▄      ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "    ▀ ▀  ▀ ▀     ", Color.YELLOWGREEN);
            return chaines;
        }
        else{
            EnsembleChaines chaines = new EnsembleChaines();
            chaines.ajouteChaine((int)this.posX, (int)posY+4, "      ▄██▄       ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+3, "    ▄██████▄     ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+2, "   ███▄██▄███    ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+1, "  ▀▄  ▀▄▄▀  ▄▀   ", Color.YELLOWGREEN);
            chaines.ajouteChaine((int)this.posX, (int)posY+0, "    ▀▀▀  ▀▀▀     ", Color.YELLOWGREEN);
            return chaines;
        }
        
    }
}